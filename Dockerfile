FROM nantou/cute_ub1804.base

COPY Anaconda3-2019.03-Linux-x86_64.sh /tmp

RUN bash /tmp/Anaconda3-2019.03-Linux-x86_64.sh -b -p /opt/conda
RUN echo "export PATH=/opt/conda/bin:$PATH" >> /etc/bash.bashrc

ENV LANG C.UTF-8 LC_ALL=C.UTF-8
RUN rm /tmp/Anaconda3-2019.03-Linux-x86_64.sh
RUN /opt/conda/bin/conda install -c conda-forge jupyterhub  && \
/opt/conda/bin/conda install notebook
RUN  mkdir /etc/jupyterhub && /opt/conda/bin/jupyterhub --generate-config && mv jupyterhub_config.py  /etc/jupyterhub

RUN mkdir /workspace
WORKDIR /workspace




